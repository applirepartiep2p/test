import java.net.*;

public class Pair {
	private TableRoutage table;
	private String ip;

	public Pair() {
		try {
			this.table = new TableRoutage(Inet4Address.getLocalHost().getHostAddress());
			this.ip = Inet4Address.getLocalHost().getHostAddress();
		} catch (Exception e){
			System.out.println(e);
		}	
	}

	public String getIP() {
		return this.ip;
	}

	public void changeSuccesseur(String ip) {
		this.table.setSuccesseur(ip);
	}

	public void changePredecesseur(String ip) {
		this.table.setPredecesseur(ip);
	}

	public String getPredecesseurLePlusProche(String ip) {
		return this.table.getPredecesseurLePlusProche(ip);
	}

	public String getPredecesseur(){
		return this.table.getPredecesseur();
	}

	public String getSuccesseur(){
		return this.table.getSuccesseur();
	}

	public TableRoutage getTable() {
		return this.table;
	}

	public void start() throws InterruptedException {
		// Lancement du processus d'attente de connexion
		AttenteConnexion atc = new AttenteConnexion(this);
		Thread processATC = new Thread(atc);
		processATC.start();

		// Lancement d'un moteur
		Moteur moteurCO = new Moteur(this, false);
		Thread processCO = new Thread(moteurCO);
		processCO.start();
	}

	public static void main(String[] args) {
		Pair mainP = new Pair();

		try {
			mainP.start();
		} catch (InterruptedException e) {
			System.out.println("Probleme au niveau de l'attente du processus de connexion dans le pair.");
		}
	}
}