public class Entre {
	private int hash;
	private String ip;

	public Entre(int hash, String ip) {
		this.hash = hash;
		this.ip = ip;
	}

	public void setIP(String ip) {
		this.ip = ip;
	}

	public void setHash(int hash) {
		this.hash = hash;
	}

	public String getIP() {
		return this.ip;
	}

	public int getHash() {
		return this.hash;
	}

	public String toString() {
		return "[" + this.hash + " : " + this.ip + "]";
	}
}