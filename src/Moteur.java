import java.net.*;
import java.io.*;
import java.util.*;

public class Moteur implements Runnable {
	private Pair pair;
	private Socket socket;
	private Boolean ecoute;
	private Scanner sc;

	public Moteur(Pair pr, Boolean ecoute){
		this.pair = pr;
		this.ecoute = ecoute;
		this.sc = new Scanner(System.in);
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public void changeSucc(String ip){
		this.pair.changeSuccesseur(ip);
	}

	public void changePred(String ip){
		this.pair.changePredecesseur(ip);
	}

	/**
	  * Rafraichi la table du pari actuel
	  *
	  * @param ip l'addr ip du pair qui veut se rajouter
	  */
	public void refreshTable(String ip) throws IOException {
		String plpp = this.pair.getPredecesseurLePlusProche(ip);

		if (!plpp.equals("0")){

			/*
			p2 : pair voulant se rajouter
			p3 : prédecesseur le plus proche de 
			*/
			System.out.println("Je suis le pred le plus proche");
			Connexion conn_p2 = new Connexion(ip, 8000);
			Connexion conn_p3 = new Connexion(plpp, 8000);
			conn_p2.envoiDonnees("cp:"+plpp);
			conn_p3.envoiDonnees("gs:0");
			String ipPredP3 = conn_p3.recoitDonnees();
			Connexion conn_p4 = new Connexion(ipPredP3, 8000);
			conn_p2.envoiDonnees("cs:"+ipPredP3);
			conn_p4.envoiDonnees("cp:"+ip);
			conn_p3.envoiDonnees("cs:"+ip);

			conn_p2.deconnexion();
			conn_p3.deconnexion();
			conn_p4.deconnexion();
		} else {
			System.out.println("Je cherche encore le plpp");
			Connexion conn = new Connexion(plpp, 8000);
			conn.envoiDonnees("rt:"+ip);
			conn.deconnexion();
		}
	}

	public void runEcoute() throws IOException {
		// Source donnée d'où viennent les connexions
		InputStream input_raw = this.socket.getInputStream();
		BufferedReader input = new BufferedReader(new InputStreamReader(input_raw));
		
		// On défini notre contenant de commande
		String ligne = "go";
		
		// Interpretation des commandes
		while (!ligne.equals("close")){
			//Attente d'une entrée de la connection
			ligne = input.readLine();

			//Séparation entre commande et adresse ip cible
			System.out.println(ligne+";");
			String[] tab_cmd = ligne.split(":");

			String commande = tab_cmd[0];
			String ip = "";

			if(!commande.equals("close"))
				ip = tab_cmd[1];


			if (commande.equals("cs")){ //Commande 'cs' = Changer le successeur
				System.out.println("-- Commande C.S. reçue --");
				this.pair.changeSuccesseur(ip);
			} else if (commande.equals("cp")){ //Commande 'cp' = Changer le prédécesseur
				System.out.println("-- Commande C.P. reçue --");
				this.pair.changePredecesseur(ip);
			} else if (commande.equals("rt")){ //Commande 'rt' = rafraichir la table suivant l'ip voulant s'ajouter
				System.out.println("-- Commande R.T. reçue --");
				this.refreshTable(ip);
			} else if (commande.equals("gs")){ //Commande 'gs' = Envoyer le successeur du pair
				System.out.println("-- Commande G.S. reçue --");
				OutputStream fluxSortie = this.socket.getOutputStream();
				PrintWriter sortie = new PrintWriter(fluxSortie , true );
				sortie.println(this.pair.getSuccesseur());
			} else if(commande.equals("msg")) {
				System.out.println("Message reçu : " + ip);
			}
		}
	}

	public void runConnexion() {
		String ligne = new String();
		do{
			ligne = this.sc.nextLine();
			String[] tableau_commande = ligne.split(":");
			if (tableau_commande[0].equals("cs") && (tableau_commande.length == 3)){ //Commande 'cs' = Changer le successeur
				System.out.println("-- Commande C.S. envoyée --");
				Connexion conn = new Connexion(tableau_commande[1], 8000);
				conn.envoiDonnees("cs:"+tableau_commande[2]);
				conn.deconnexion();
			} else if (tableau_commande[0].equals("cp") && (tableau_commande.length == 3)){ //Commande 'cp' = Changer le prédécesseur
				System.out.println("-- Commande C.P. envoyée --");
				Connexion conn = new Connexion(tableau_commande[1], 8000);
				conn.envoiDonnees("cp:"+tableau_commande[2]);
				conn.deconnexion();
			} else if (tableau_commande[0].equals("rt") && (tableau_commande.length == 3)){ //Commande 'rt' = rafraichir la table suivant l'ip voulant s'ajouter
				System.out.println("-- Commande R.T. envoyée --");
				Connexion conn = new Connexion(tableau_commande[1], 8000);
				conn.envoiDonnees("rt:"+tableau_commande[2]);
				conn.deconnexion();
			} else if (tableau_commande[0].equals("msg") && (tableau_commande.length == 3)) {
				System.out.println("-- Commande MSG envoyée --");
				Connexion conn = new Connexion(tableau_commande[1], 8000);
				conn.envoiDonnees("msg:"+tableau_commande[2]);
				conn.deconnexion();
			} else if (tableau_commande[0].equals("print")) {
				System.out.println(this.pair.getTable().toString());
			} else {
				System.out.println("--------------------\nPas la bonne commande !\n--------------------");
			}
		} while(!ligne.equals("quit"));

		System.exit(0);
	}

	public void run(){
		try {
			if(this.ecoute) {
				if(this.socket != null) {
					System.out.println("Pair pret à écouter");
					this.runEcoute();
				} else {
					System.out.println("Erreur socket manquant pour etablir la connexion");
				}
			} else {
				System.out.println("Pair pret à envoyer");
				this.runConnexion();
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}