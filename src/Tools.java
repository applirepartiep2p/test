import java.util.*;

public class Tools{
	public static int hashage(String ip){
		int res = ip.hashCode();
		int ascii = 1;

		ascii = res%10000;
		
		return Math.abs((ascii));
	}

	public static int max_list(List<Integer> list) {
		int max = 0;

		for(Iterator<Integer> it = list.iterator(); it.hasNext(); ) {
			int value = it.next();

			max = (value > max) ? value : max;
		}

		return max;
	}
}