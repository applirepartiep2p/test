@echo off

title Reseau Pair a Pair
color 1f

if "%1" == "" goto AIDE
if "%1" == "-h" goto AIDE

echo Classe detectees
java Getsrc

echo Compilation...
javac @options @classes

if %ERRORLEVEL% GTR 0 goto ERREUR

echo Compilation reussie
echo Voulez vous lancer le programme ? (y/n)

set /P rep=">"

if %rep% NEQ y goto FIN
cls
java -cp bin %1
goto FIN

:AIDE
echo Vous devez renseigner en 1er argument le nom de la classe principale a lancer.
goto FIN2

:ERREUR
echo Erreur de compilation

:FIN
echo Fin...
:FIN2