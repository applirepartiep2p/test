import java.io.*;

import java.net.*;
import java.util.Scanner;

public class AttenteConnexion implements Runnable {
	private Pair pair;

	public AttenteConnexion(Pair pair) {
		this.pair = pair;
	}

	public void run() {
		System.out.println("Thread en attente de connexion");
		try {
			ServerSocket serveur = new ServerSocket(8000);
			
			while (true) {
				Socket client = serveur.accept();
				System.out.println("Une connexion a été établie.");
				
				// Envoyer le client au routeur
				Moteur moteur = new Moteur(this.pair, true);
				moteur.setSocket(client);
				Thread thread = new Thread(moteur);
				thread.start();
			}			
		} catch(IOException e) {
			System.out.println("Une erreur est survenue : " + e.getMessage());
		}
	}
}