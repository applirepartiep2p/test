import java.util.*;

public class TableRoutage {
	private String ip_pair;
	private Entre successeur;
	private Entre predecesseur;
	private HashMap<Integer, String> table;

	public TableRoutage(String ip_pair) {
		this.ip_pair = ip_pair;

		this.successeur = new Entre(Tools.hashage(ip_pair), ip_pair);
		this.predecesseur = new Entre(Tools.hashage(ip_pair), ip_pair);
		this.table = new HashMap<Integer, String>();
		this.table.put(Tools.hashage(ip_pair), ip_pair);
	}

	public void setSuccesseur(String ip) {
		this.successeur.setHash(Tools.hashage(ip));
		this.successeur.setIP(ip);
	}

	public void setPredecesseur(String ip) {
		this.predecesseur.setHash(Tools.hashage(ip));
		this.predecesseur.setIP(ip);
	}

	public String getPredecesseur(){
		return this.predecesseur.getIP();
	}

	public String getSuccesseur(){
		return this.successeur.getIP();
	}

	public void addEntree(String ip) {
		this.table.put(Tools.hashage(ip), ip);
	}

	public String toString() {
		StringBuffer str = new StringBuffer();

		System.out.println("--------");

		str.append("Successeur : \t[").append(this.predecesseur.getHash()).append(" \t| ").append(this.predecesseur.getIP()).append("]\n");
		str.append("Prédécesseur : \t[").append(this.successeur.getHash()).append("\t| ").append(this.successeur.getIP()).append("]\n");
		
		List<Integer> tab = new ArrayList<Integer>(this.table.keySet());

		Collections.sort(tab);

		for(int hash : tab) {
			String ip = this.table.get(hash);
			str.append(hash).append("\t:\t").append(ip).append("\n");			
		}
		return str.toString();
	}

	public String getPredecesseurLePlusProche(String ip) {
		int hash = Tools.hashage(ip);
		int max_hash;
		List<Integer> hashs = new ArrayList<Integer>(this.table.keySet());
		
		if(!ip.equals(this.ip_pair)) { // Si l'ip du pair qu'on veut faire rentrer n'est pas egale à l'ip du pair 
			do {
				max_hash = Tools.max_list(hashs);

				if(max_hash >= hash)
					hashs.remove((Object) max_hash);

			} while(max_hash >= hash);

			String yo = this.table.get(Tools.max_list(hashs));

			if(yo == null) { // Si trouvé
				System.out.println("Trop cool");
				return this.ip_pair;
			} else {
				return yo;
			}
		} else {
			String ipar = this.predecesseur.getIP();

			if(ipar.equals(this.ip_pair)) {
				return "0";
			} else {
				return ipar;
			}
		}
	}
}

/*

P1 : Pair actuellement sur le réseau
P2 : Pair qui veut rentrer sur le réseau et  n            qui connaît P1
P3 : prédécesseur direct du nouveau pair P2

1 - P2 demande à P1 pour que P2 soit dans le réseau
2 - P1 cherche P3
3 - Dire à P2 que P3 est son prédécesseur et l'ancien successeur de P3 est celui de P2
	hyt .pred(P2) = hash(P3)
	succ(P2) = hash(succ(P3))
4 - Dire à l'ancien successeur de P3 que son prédecesseur est P2
	pred(succ(P3)) = hash(P2)
5 - Le successeur de P3 devient P2
	succ(P3) = hash(P2)





1 - Trouver le préd du nouveau pair
En gros si hash(du pair qui recoi la demande d'inté dans le rsx) <= hash(d'un pair) < hash(du nouveau) 
Si le pair qui reçoi le msg est le prédécesseur direct du nouveau Pair ALORS : (sinon retourner à 1)

2 - lui dire que je suis ton préd et que mon anciens succ est ton succ

3 - dire à l'ancien succ, ton pred c'est le nouveau pair

*/