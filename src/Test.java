public class Test {
	public static void main(String[] args) {
		String ip_rsx = "192.168.0.";

		TableRoutage table = new TableRoutage("192.168.0.0");

		System.out.println("Lol1 : " + Tools.hashage("0.0.0.0"));
		System.out.println("Lol2 : " + Tools.hashage("255.255.255.255"));

		for (int i = 1; i < 20; i++) {
			table.addEntree(ip_rsx + i);
		}

		System.out.println(table.toString());

		String ip = "192.168.0.0";

		System.out.println("Pred le plus proche : " + table.getPredecesseurLePlusProche(ip) + "\thash : " + Tools.hashage(ip));
	}
}