#!/bin/bash

clear

rm -f -r src/.*

if [ "$1" != "-s" ]
then
	java Getsrc
fi

echo Compilation...
javac @options @classes

if [ $? = 0 ]
then
	echo Aucune erreur !
	echo Appuyez sur entrer pour continuer
	read p
	clear
	java -cp bin Pair
fi