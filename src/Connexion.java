import java.io.*;

import java.net.Socket;
import java.util.Scanner;

public class Connexion{
	Socket mySocket;

	public Connexion(String ip, int port){
		try {
			this.mySocket = new Socket (ip, port);
		} catch(IOException e){
			e.printStackTrace();
		}

	}
	public boolean deconnexion(){
		boolean test=false;
		
		this.envoiDonnees("close");
		
		try{
			this.mySocket.close();
			test = true;
		} catch(Exception e){
			e.printStackTrace();
		}


		return test;
	}

	public boolean envoiDonnees(String donnee){
		boolean test=false;
		
		try {
			OutputStream fluxSortie = mySocket.getOutputStream();
			System.out.println(fluxSortie == null);
			PrintWriter sortie = new PrintWriter(fluxSortie , true);
			sortie.println(donnee);

			test = true;
		} catch(IOException e) {
			e.printStackTrace();
			System.out.println("Le système a été confronté à une erreur.");
			test = false;
		}

		return test;
	}

	public String recoitDonnees() throws IOException {
		InputStream fluxEntree=mySocket.getInputStream();
		boolean test = false;
		BufferedReader input =  new BufferedReader(new InputStreamReader(fluxEntree));

		return input.readLine();
	}
}